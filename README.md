### Setup

```
npm install
```

### Start server

```
PORT=3000 npm start
```

> try: http://localhost:3000/get-follow-count/google

### Run tests

```
npm test
```

##### Notes:

- It uses in-memory db. Data is stored in data/db.json
- If Insta user data is in DB, it returns stored, if not fetches and stores.
- Stores created timestamp, later can be used to cleanup old
- Basic error handling in place
