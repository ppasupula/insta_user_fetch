const express = require("express");

const { getFollowersCount } = require("./src/services/follower.service");

const PORT = process.env.PORT || 3000;
const app = express();

app.get("/get-follow-count/:id", async (req, res, next) => {
  let data;
  try {
    data = await getFollowersCount(req.params.id);
  } catch (error) {
    next(error);
    return;
  }

  res.json(data);
});

app.use(function(err, req, res, next) {
  res.json({ message: err.message });
});

app.listen(PORT, () => console.log("Listening on ", PORT));
