const axiosMock = {
  get: jest.fn()
};

module.exports = axiosMock;
