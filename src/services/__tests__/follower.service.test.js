const axios = require("axios");
const fs = require("fs");

const { getFollowersCount } = require("../follower.service");

jest.mock("axios");

describe("getFollowersCount", () => {
  it("requires username", () => {
    expect(getFollowersCount()).rejects.toEqual(new Error("INVALID_USERNAME"));
  });

  it("fetches insta page", async () => {
    jest.spyOn(axios, "get").mockResolvedValue({
      data: {}
    });

    try {
      await getFollowersCount("testnameee123");
    } catch (e) {}

    expect(axios.get).toHaveBeenCalledTimes(1);
  });

  it("returns error when insta page not found", async () => {
    jest.spyOn(axios, "get").mockResolvedValue({
      data: {}
    });

    expect(getFollowersCount("testnameee123")).rejects.toEqual(
      new Error("INVALID_USERNAME")
    );
  });

  it("returns insta user data", async () => {
    const pageData = fs
      .readFileSync(
        process.cwd() + "/src/services/__tests__/instagram_google.html"
      )
      .toString();
    jest.spyOn(axios, "get").mockResolvedValue({
      data: pageData
    });

    const result = await getFollowersCount("google");

    expect(result.fullName).toEqual("Google");
    expect(result.followerCount).toEqual(10792550);
    expect(result.username).toEqual("google");
    expect(result.capturedAt).toEqual(expect.any(String));
  });
});
