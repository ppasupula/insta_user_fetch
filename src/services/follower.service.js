const axios = require("axios");
const R = require("ramda");

const { db } = require("../db.js");

const INSTA_ROOT_URL = "https://www.instagram.com";

exports.getFollowersCount = async username => {
  if (!username) throw new Error("INVALID_USERNAME");

  let instagramUser = db
    .get("instagramUsers")
    .find({ username })
    .value();

  if (!instagramUser) {
    instagramUser = await fetchInstaData(username);
    if (instagramUser) {
      db.get("instagramUsers")
        .push(instagramUser)
        .write();
    }
  }

  return instagramUser;
};

const fetchInstaData = async username => {
  let resp = null;
  try {
    resp = await axios.get(`${INSTA_ROOT_URL}/${username}`);
  } catch (error) {
    console.log("follower.service: Failed to fetch. Status:", error.message);
    throw new Error("INVALID_USERNAME");
  }

  const parsed = extractSharedDataFromBody(resp.data);

  const userData = R.path(
    ["entry_data", "ProfilePage", 0, "graphql", "user"],
    parsed
  );

  if (!userData) throw new Error("INVALID_USERNAME");

  return {
    fullName: R.path(["full_name"], userData),
    followerCount: R.path(["edge_followed_by", "count"], userData),
    username: username,
    capturedAt: new Date()
  };
};

const extractSharedDataFromBody = body => {
  const regex = /window\._sharedData = (.*);<\/script>/;
  const match = regex.exec(body);

  if (!match || typeof match[1] === "undefined") {
    return "";
  }

  return JSON.parse(match[1]);
};
