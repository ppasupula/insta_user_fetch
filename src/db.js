const low = require("lowdb");

const FileSync = require("lowdb/adapters/FileSync");

const adapter = new FileSync("./data/db.json");
const db = low(adapter);

db.defaults({ instagramUsers: [], count: 0 }).write();

module.exports = { db };
